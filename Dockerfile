FROM php:7.3-cli

RUN apt-get update && \
    apt-get -y install \
        cron

COPY crontab /etc/cron.d/ddns
COPY updater.php /app/updater.php
COPY config.json /app/config.json
COPY init.sh /app/init.sh

RUN chown -R www-data:www-data /app

CMD ["/app/init.sh"]