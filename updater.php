<?php

define('CONFIG_FILE', 'config.json');
$config = json_decode(file_get_contents(CONFIG_FILE), true);
$ip = file_get_contents($config['lookuper']);
$old_ip = file_exists($config['ip']) ? file_get_contents($config['ip']) : '';
if ($ip == $old_ip) {
    file_put_contents($config['log'], date('Y-m-d H:i:s') . " $old_ip == $ip not changed\r\n", FILE_APPEND);
} else {
    file_put_contents($config['log'], date('Y-m-d H:i:s') . " $old_ip != $ip changed\r\n", FILE_APPEND);
    foreach ($config['domains'] as $domain => $records) {
        foreach ($records as $record) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://pddimp.yandex.ru/api2/admin/dns/edit');
            $post = http_build_query([
                'domain' => $domain,
                'record_id' => $record['id'],
                'content' => $ip,
                'ttl' => $record['ttl'],
                'subdomain' => $record['subdomain']
            ]);
            file_put_contents($config['log'], date('Y-m-d H:i:s') . " $post\r\n", FILE_APPEND);
            curl_setopt_array($curl, [
                CURLOPT_TIMEOUT => 30,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $post,
                CURLOPT_HTTPHEADER => [
                    'PddToken: ' . $config['token']
                ]
            ]);
            $response = curl_exec($curl);
            file_put_contents($config['log'], date('Y-m-d H:i:s') . " $response\r\n", FILE_APPEND);
            curl_close($curl);
            echo $response . "\r\n";
        }
    }
    file_put_contents($config['ip'], $ip);
}